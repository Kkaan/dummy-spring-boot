FROM maven:3-jdk-8 as builder
COPY . /app
WORKDIR /app
RUN mvn  -Dmaven.test.skip=true package
FROM openjdk:8-alpine
WORKDIR /app
COPY --from=builder /app/target/*.jar ./app.jar
CMD ["java","-jar","app.jar"]